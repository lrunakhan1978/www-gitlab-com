groups:
  - group: GitLab features and benefits
    questions:
      - question: What are the differences between Free Premium, and Ultimate plans?
        answer: >-
          All of the features and benefits of the different GitLab offerings can be found on the [pricing page](/pricing/).
      - question: What is the difference between GitLab and other Git solutions?
        answer: >-
          You can see all the differences between GitLab and other popular Git solutions on our [competitive comparison pages](/devops-tools/).
  - group: Payments and discounts
    questions:
      - question: How much does a GitLab license cost?
        answer: >-
          Subscription information is listed on our [pricing page](/pricing/).
      - question: How do I upgrade from GitLab Free to one of the paid subscriptions?
        answer: >-
          If you want to upgrade from GitLab Free to one of the paid tiers, follow the [guides in our documentation](https://docs.gitlab.com/ee/update/README.html#community-to-enterprise-edition).
      - question: How do I purchase additional users for a license I already bought?
        answer: >-
          You have a few options. You can add users to your subscription any time during the subscription period. You can log in to your account via the [GitLab Customers Portal](https://customers.gitlab.com/) and add more seats or by contacting renewals@gitlab.com for a quote. In either case, the cost will be prorated from the date of quote/purchase through the end of the subscription period. You may also pay for the additional licenses per our true-up model.
      - question: Do you have special pricing for open source projects, startups, or educational institutions?
        answer: >-
          Yes! We provide free Ultimate licenses to qualifying open source projects, startups, and educational institutions. Find out more by visiting our [GitLab for Open Source](/solutions/open-source/), [GitLab for Startups](/solutions/startups/), and [GitLab for Education](/solutions/education/) program pages.
      - question: Do you offer any discounts for GitLab?
        answer: >-
          GitLab Free is open source and completely free for anyone to use. Please contact GitLab sales for more information.
  - group: Installation and Migration
    questions:
      - question: How do I migrate to GitLab from another Git tool?
        answer: >-
          See all the project migration instructions for popular version control systems in [our documentation](https://docs.gitlab.com/ee/user/project/import/index.html).
      - question: How do I install GitLab?
        answer: >-
          GitLab can be installed in most GNU/Linux distributions, and with several cloud providers. Find out about installing GitLab in [our documentation](https://docs.gitlab.com/ee/install/).
      - question: What tools does GitLab integrate with?
        answer: >-
          GitLab offers a number of third-party integrations. Learn more about available services and how to integrate them in [our documentation](https://docs.gitlab.com/ee/integration/README.html).
